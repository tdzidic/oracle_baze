select distinct b.godina, a.aop, a.opis, sum(st.iznos) "iznos"
from  dwh_fina.v_gfipod_zaglav z
    , dwh_fina.v_gfipod_baza b    
    , (select * from  dwh_fina.v_gfipod_stavka_tekuca
      unpivot (iznos for aop_id IN ("AOP001", "AOP002", "AOP003", "AOP004", "AOP005", "AOP006", "AOP007", "AOP008", "AOP009", "AOP010", "AOP011", "AOP012", "AOP013", "AOP014", "AOP015", "AOP016", "AOP017", "AOP018", "AOP019", "AOP020", "AOP021", "AOP022", "AOP023", "AOP024", "AOP025", "AOP026", "AOP027", "AOP028", "AOP029", "AOP030", "AOP031", "AOP032", "AOP033", "AOP034", "AOP035", "AOP036", "AOP037", "AOP038", "AOP039", "AOP040", "AOP041", "AOP042", "AOP043", "AOP044", "AOP045", "AOP046", "AOP047", "AOP048", "AOP049", "AOP050", "AOP051", "AOP052", "AOP053", "AOP054", "AOP055", "AOP056", "AOP057", "AOP058", "AOP059", "AOP060", "AOP061", "AOP062", "AOP063", "AOP064", "AOP065", "AOP066", "AOP067", "AOP068", "AOP069", "AOP070", "AOP071", "AOP072", "AOP073", "AOP074", "AOP075", "AOP076", "AOP077", "AOP078", "AOP079", "AOP080", "AOP081", "AOP082", "AOP083", "AOP084", "AOP085", "AOP086", "AOP087", "AOP088", "AOP089", "AOP090", "AOP091", "AOP092", "AOP093", "AOP094", "AOP095", "AOP096", "AOP097", "AOP098", "AOP099", "AOP100", "AOP101", "AOP102", "AOP103", "AOP104", "AOP105", "AOP106", "AOP107", "AOP108", "AOP109", "AOP110", "AOP111", "AOP112", "AOP113", "AOP114", "AOP115", "AOP116", "AOP117", "AOP118", "AOP119", "AOP120", "AOP121", "AOP122", "AOP123", "AOP124", "AOP125", "AOP126", "AOP127", "AOP128", "AOP129", "AOP130", "AOP131", "AOP132", "AOP133", "AOP134", "AOP135", "AOP136", "AOP137", "AOP138", "AOP139", "AOP140", "AOP141", "AOP142", "AOP143", "AOP144", "AOP145", "AOP146", "AOP147", "AOP148", "AOP149", "AOP150", "AOP151", "AOP152", "AOP153", "AOP154", "AOP155", "AOP156", "AOP157", "AOP158", "AOP159", "AOP160", "AOP161", "AOP162", "AOP163", "AOP164", "AOP165", "AOP166", "AOP167", "AOP168", "AOP169", "AOP170", "AOP171", "AOP172", "AOP173", "AOP174", "AOP175", "AOP176", "AOP177", "AOP178", "AOP179", "AOP180", "AOP181", "AOP182", "AOP183", "AOP184", "AOP185", "AOP186", "AOP187", "AOP188", "AOP189", "AOP190", "AOP191", "AOP192", "AOP193", "AOP194", "AOP195", "AOP196", "AOP197", "AOP198", "AOP199", "AOP200", "AOP201", "AOP202", "AOP203", "AOP204", "AOP205", "AOP206", "AOP207", "AOP208", "AOP209", "AOP210", "AOP211", "AOP212", "AOP213", "AOP214", "AOP215", "AOP216", "AOP217", "AOP218", "AOP219", "AOP220", "AOP221", "AOP222", "AOP223", "AOP224", "AOP225", "AOP226", "AOP227", "AOP228", "AOP229", "AOP230", "AOP231", "AOP232", "AOP233", "AOP234", "AOP235", "AOP236", "AOP237", "AOP238", "AOP239", "AOP240", "AOP241", "AOP242", "AOP243", "AOP244", "AOP245", "AOP246", "AOP247", "AOP248", "AOP249", "AOP250", "AOP251", "AOP252", "AOP253", "AOP254", "AOP255", "AOP256", "AOP257", "AOP258", "AOP259", "AOP260", "AOP261", "AOP262", "AOP263", "AOP264", "AOP265", "AOP266", "AOP267", "AOP268", "AOP269", "AOP270", "AOP271", "AOP272", "AOP273", "AOP274", "AOP275", "AOP276", "AOP277", "AOP278", "AOP279", "AOP280", "AOP281", "AOP282", "AOP283", "AOP284", "AOP285", "AOP286", "AOP287", "AOP288", "AOP289", "AOP290", "AOP291", "AOP292", "AOP293", "AOP294", "AOP295", "AOP296", "AOP297", "AOP298", "AOP299", "AOP300", "AOP301", "AOP302")
      )) st
    , dwh_fina.v_gfipod_aop a
    , RPS_SEKTORI_2010 rps    
where  z.gfipod_baza_id = b.id
    and a.gfipod_baza_id = b.id 
    and z.id = st.gfipod_zaglav_id
    and z.oib=rps.oib
    and z.SIFRA_DOSTAVE = 1
    and a.aop =to_number(substr(st.AOP_ID,-3))
    and b.godina = 2017
    
    -- ovo je za vađenje pojedinačnog obrasca
    and z.oib = '47445593925'
    
    --ovo je za agregiranje po sektoru
    --and rps.SK10_SIFRA like '11%'
    --AND (rps.DATUM_DO > '31-12-2017' OR rps.DATUM_DO IS NULL)
    --and (rps.status_sektora like '1' or rps.status_sektora like '2')
    
group by (b.godina, a.aop, a.opis)    
order by to_NUMBER(a.aop);
